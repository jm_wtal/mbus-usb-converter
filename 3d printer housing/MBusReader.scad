$width=41.2;
$length=74.2;
$wall=2;
$offset_wall=0.5;
$height_bottom=18;
$standoff=2.5;
$pcb_height=1.6;
$rs232_width=15;
$usb_width=20;
$mbus_width=10;
$terminal_height=7.2;
$fn=60;

// CASE
difference(){
    translate([-$wall-$offset_wall, -$wall-$offset_wall, -$wall]){
        
        difference(){
            cube([$width+2*($wall+$offset_wall), 
                 $length+2*($wall+$offset_wall), 
                 $height_bottom+$wall]);
            
            // RS232
            translate([($width+2*($wall+$offset_wall))/2-$rs232_width/2,
                        0,
                        $wall+$pcb_height+$standoff])
                cube([$rs232_width,$wall,$terminal_height]);
            
            // USB
            translate([($width+2*($wall+$offset_wall))-$wall,
                       $wall+$offset_wall+19.5,
                       $wall+$pcb_height+$standoff])
                cube([$wall,$usb_width,$terminal_height]);

            // 34V
            translate([0,
                       $wall+$offset_wall+31,
                       $wall+$pcb_height+$standoff])
                cube([$wall,$mbus_width,$terminal_height]);
            
            // MBUS
            translate([($wall+$offset_wall)+10,
                        $length+2*($wall+$offset_wall)-$wall,
                        $wall+$pcb_height+$standoff])
                cube([$mbus_width,$wall,$terminal_height]);
        }
    }
    
    translate([-$offset_wall,-$offset_wall,0])
        cube([$width+2*$offset_wall, $length+2*$offset_wall, $height_bottom]);
}

// TOP
translate([-$width-15,-$wall-$offset_wall,-$wall])
    difference(){
        union(){
            cube([$width+2*($wall+$offset_wall), 
                  $length+2*($wall+$offset_wall), 
                  $wall]);
            
            $offset_w_l=0.2;
            translate([$wall+$offset_w_l,$wall+$offset_w_l,$wall]){
                  cube([$width+2*($offset_wall)-$offset_w_l, 
                  $length+2*($offset_wall)-$offset_w_l, 
                  $wall]);
            }
        }
        
        // LED
        translate([$wall+$offset_wall+12.5,$wall+$offset_wall+65.5,0])
            cylinder(d=4,h=2*$wall);
        
    }


// stand offs
union(){
    translate([4,$length-4,0]){
        cylinder(d=5.5, h=$standoff);    
        translate([0,0,$standoff]){
            cylinder(d=3,h=$pcb_height);
        }
    }

    translate([$width-4,$length-4,0]){
        cylinder(d=5.5, h=$standoff);    
        translate([0,0,$standoff]){
            cylinder(d=3,h=$pcb_height);
        }
    }

    translate([6.5,4,0]){
        cylinder(d=5.5, h=$standoff);    
        translate([0,0,$standoff]){
            cylinder(d=3,h=$pcb_height);
        }
    }

    translate([$width-6.5,4,0]){
        cylinder(d=5.5, h=$standoff);    
        translate([0,0,$standoff]){
            cylinder(d=3,h=$pcb_height);
        }
    }
}