# The M-Bus-TTL (USB/RS232) Converter

![MBus-Converter](https://pc-projekte.lima-city.de/media/images/m-bus_k_3d_seit_schrift.jpg "M-Bus Converter")


## EN
Simple low-buget M-Bus (Meter-Bus) - USB (RS232/TTL) - Converter for reading M-Bus meters.           

The M-Bus is a fieldbus for consumption data acquisition. The data is transferred serially from the connected measuring devices (slaves) to a master on a reverse polarity protected two-wire line. The master queries the meters via the bus. Power can be supplied to the slaves via the bus. The master can be an independent device, but also a PC or RaspberryPi with a **level converter**. 
This project provides a simple low-budget M-Bus level converter to collect consumption data e.g. in a home automation system.

## DE
Einfacher low-buget M-Bus (Meter-Bus) -USB (RS232/TTL) - Konverter zum Auslesen von M-Bus- Zählern.

Der M-Bus ist ein Feldbus für die Verbrauchsdatenerfassung. Die Übertragung erfolgt seriell auf einer verpolungssicheren Zweidrahtleitung von den angeschlossenen Messgeräten (Slaves) zu einem Master. Der Master fragt über den Bus die Zähler ab. Die Stromversorgung der Slaves kann über den Bus erfolgen. Der Master kann ein eigenständiges Gerät sein, aber auch ein PC oder RaspberryPi mit einem **Pegelwandler**. 
Dieses Projekt stellt einen einfachen Low-Budget-M-Bus-Pegelwandler zur Verfügung, um Verbrauchsdaten z.B. in einer Hausautomatisierung zu erfassen.

